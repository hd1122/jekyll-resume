# 期末自评总结
## 提交commits数量及频率
 [gitee commit](https://gitee.com/hd1122/jekyll-resume/commits/master)  
 [gitee 概览](https://gitee.com/hd1122)  
 我在gitee上commit提交次数为38次
 
 ## 实操项目
 文档格式： [md语法介绍](https://gitee.com/hd1122/jekyll-resume/blob/master/md%E8%AF%AD%E6%B3%95%E7%9A%84%E6%80%BB%E7%BB%93.md) 
  [md语法实践展示](https://gitee.com/hd1122/jekyll-resume/blob/master/%E4%BD%BF%E7%94%A8Cookie%E7%9A%84%E7%8A%B6%E6%80%81%E7%AE%A1%E7%90%86.md)    
用途：通过简单的标记语法，它可以使普通文本内容具有一定的格式  
 意义：兼容所有的文本编辑器，可读，直观，适合所有人的写作语言 减少排版所带来的麻烦，利于文本在各个格式的转换
   
   文档格式：[json介绍](https://gitee.com/hd1122/jekyll-resume/blob/master/json.md)  [json](https://gitee.com/hd1122/jekyll-resume/blob/master/territories.json)  
   用途： 用于存储和传输数据的格式，通常用于服务端向网页传递数据 。 
   意义：文本可以被任何编程语言读取及作为数据格式传递  
     
文档格式： [yaml](https://gitee.com/hd1122/jekyll-resume/blob/master/_config.yml)  
用途：YAML的语法和其他高阶语言类似，并且可以简单表达清单、散列表，标量等资料形态。适合描述程序所使用的数据结构，特别是脚本语言  
意义： 丰富的表达能力与可扩展性，具有 XML 同样的优点，但比 XML 更加简单、敏捷等 
  
  文档格式： [tsv](https://gitee.com/hd1122/jekyll-resume/blob/master/list.tsv)  
  用途： 将数据转换成表格形式 
   意义：  使数据收集更加清晰 文本表达更加清楚 
  
文档格式： [csv](https://gitee.com/hd1122/jekyll-resume/blob/master/mainland.csv)  
用途：在不兼容的格式上进行操作，在程序之间转移表格数据  
意义：  文件结构简单，基本上和文本的差别不大；可以和microExcle进行转换，可以减少存储信息的容量，这样有利于网络传输以及客户端的再处理
  
  参与科技与共享文化体验展示  
   [网络素养读书笔记](https://gitee.com/hd1122/jekyll-resume/blob/master/%E7%BD%91%E7%BB%9C%E7%B4%A0%E5%85%BB.md)  
  
  HTTP及TCP/IP协议体验展示  
 [课堂笔记](https://gitee.com/hd1122/jekyll-resume/blob/master/HTTP%E5%8F%8ATCP/IP%E5%8D%8F%E8%AE%AE%E7%AC%94%E8%AE%B0.md)
  