2.8　使用 Cookie 的状态管理
-------------------

HTTP 是无状态协议，它不对之前发生过的请求和响应的状态进行管理。也就是说，无法根据之前的状态进行本次的请求处理。

保留无状态协议这个特征的同时又要解决类似的矛盾问题，于是引入了 Cookie 技术。Cookie 技术通过在请求和响应报文中写入 Cookie 信息来控制客户端的状态。

Cookie 会根据从服务器端发送的响应报文内的一个叫做 Set-Cookie 的首部字段信息，通知客户端保存 Cookie。当下次客户端再往该服务器发送请求时，客户端会自动在请求报文中加入 Cookie 值后发送出去。

服务器端发现客户端发送过来的 Cookie 后，会去检查究竟是从哪一个客户端发来的连接请求，然后对比服务器上的记录，最后得到之前的状态信息。

上图展示了发生 Cookie 交互的情景，HTTP 请求报文和响应报文的内容如下。

1.  **请求报文（没有 Cookie 信息的状态）**
    
        GET /reader/ HTTP/1.1
        Host: hackr.jp
        *首部字段内没有Cookie的相关信息
        
    
2.  **响应报文（服务器端生成 Cookie 信息）**
    
        HTTP/1.1 200 OK
        Date: Thu, 12 Jul 2012 07:12:20 GMT
        Server: Apache
        ＜Set-Cookie: sid=1342077140226724; path=/; expires=Wed,
        10-Oct-12 07:12:20 GMT＞
        Content-Type: text/plain; charset=UTF-8
        
    
3.  **请求报文（自动发送保存着的 Cookie 信息）**
    
        GET /image/ HTTP/1.1
        Host: hackr.jp
        Cookie: sid=1342077140226724
        
    

**有关请求报文和响应报文内 Cookie 对应的首部字段，请参考之后的章节。**