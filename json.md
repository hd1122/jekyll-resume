## 什么是JSON？
- JSON 英文全称 JavaScript Object Notation
- JSON 是一种轻量级的数据交换格式。
- JSON 是独立的语言
- JSON 使用 JavaScript 语法，但是 JSON 格式仅仅是一个文本。
- 文本可以被任何编程语言读取及作为数据格式传递。
***
## JSON用途
1. 用于存储和传输数据的格式。
2. 通常用于服务端向网页传递数据。
***
## 学习所得 
1. JSON格式化后为JavaScript对象，将[Unicode CLDR zh-Hans/territories.json](https://github.com/unicode-cldr/cldr-localenames-modern/blob/master/main/zh-Hans/territories.json)
的josn内容通过[菜鸟工具 json在线解析器](https://c.runoob.com/front-end/53)进行格式化
2. 编写简单的[josn](https://gitee.com/hd1122/jekyll-resume/blob/master/territories.json)格式文件  
### 部分内容引用自课堂内容以及百度